const express = require("express");
const path = require("path");

app = express();
console.log(__dirname);

/*
app.all("/api/*", function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*.goodreads*");
    res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
    return next();
});
*/

app.use(express.static(path.join(__dirname, "../client")));
app.use("/libs", express.static(path.join(__dirname, "../bower_components")));

const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.log("Application started at %s on port %d"
			, new Date(), port);
});