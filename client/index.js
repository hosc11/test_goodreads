(function() {
    var BookApp = angular.module("BookApp", ['ngSanitize', 'xml']);

    BookApp.config(function($sceDelegateProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
          // Allow same origin resource loads.
          'self',
          // Allow loading from our assets domain.  Notice the difference between * and **.
          'https://www.goodreads.com/**'
        ]);
      });
    
    BookApp.config(function($httpProvider) {
        $httpProvider.interceptors.push('xmlHttpInterceptor');
      });
      
    var BookSvc = function($http, $q, $sce){
        var bookSvc = this;
        bookSvc.getBookInfo = function(url, key, q){
            var defer = $q.defer();
            /*
            $http.get(url, {
                params: { key:key, q:q }
            }).then(function(result){
                    console.log(result);
                    defer.resolve(result);
                }).catch(function(err){
                    defer.reject(err);
                })
            */
            var callUrl = url + "?callback=JSON_CALLBACK";
            $http({
                method: 'jsonp',
                url: url,
                params: { key:key, q:q  },
                "headers": {
                    "accept": "application/xml",
                    "content-type": "application/xml"
                  },
                  "data": {}
            }).then(function(result) {
                console.log('success');
                //console.log(result);
            }).catch(function(err) {
                console.log('Error in BookSvc.getBookInfo: %s', err);
            });

            
            return(defer.promise);
        }//bookSvc.getBookInfo
    }; //BookSvc
    BookSvc.$inject = ["$http", "$q", "$sce"];

    //var BookCtrl = function($scope,$http, BookSvc){
    var BookCtrl = function(BookSvc){    
        var bookCtrl = this;
        bookCtrl.goodreads = {
            url: "https://www.goodreads.com/search",
            key: "vqR2AIjf0ZTjzE8rq9rEA"
        };
        bookCtrl.searchStr = "";
        bookCtrl.searchResult = "";
        /*
        bookCtrl.search = function(){
            console.log(">>> Search String >>>> %s",bookCtrl.searchStr);
            $http({
                method: 'jsonp',
                url: bookCtrl.goodreads.url,
                params: { key:bookCtrl.goodreads.key, q:bookCtrl.searchStr  }
            }).then(function(data){
                $scope.data = data;
                console.log(">>>>>Data>>>>>");
                console.log($scope.data);
            }).catch(function(err){
                console.error(">>>>Error>>> %s",err);
                
            })
            */
            bookCtrl.search = function(){
                BookSvc.getBookInfo(bookCtrl.goodreads.url, bookCtrl.goodreads.key, bookCtrl.searchStr)
                    .then(function(result){
                        bookCtrl.searchResult = result;
                    }).catch(function(err){
                        console.info("Search String, %s not found", bookCtrl.searchStr);
                        console.error("Err: %s ", err);
                    })
            };   
        
    } //BookCtrl
    //BookCtrl.$inject = ["$scope", "$http", "BookSvc"];
    BookCtrl.$inject = ["BookSvc"];
    BookApp.service("BookSvc", BookSvc);
    BookApp.controller("BookCtrl", BookCtrl);
})();